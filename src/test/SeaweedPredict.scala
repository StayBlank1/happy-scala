package test
/**
 *  本类演示如何将本地数据通过一定的方法转换成 Spark DataFrame
 *
 */
object scala1 extends App {
  //读取本地数据，使其变为RDD，注意目录是用/分割。
  val analysis = sc.textFile("F:/scala_projects/hello_scala/data/chapter2/Analysis.txt")

  //注册数据存储类。
  case class SeaWeed(season: String, size: String, speed: String, mxph: Float, mn02: Float, c1: Float, n03: Float, nh4: Float, op04: Float, p04: Float, chla: Float, a1: Float, a2: Float, a3:Float, a4: Float, a5: Float, a6: Float, a7: Float)

  //读入数据保存到集合类中，然后转换成 DataFrame对象
  val seaWeed = analysis.map(_.split("\\s{3,5}")).map(p => SeaWeed(p(0), p(1), p(2), p(3).toFloat, p(4).toFloat, p(5).toFloat, p(6).toFloat, p(7).toFloat, p(8).toFloat, p(9).toFloat, p(10).toFloat, p(11).toFloat, p(12).toFloat, p(13).toFloat, p(14).toFloat, p(15).toFloat, p(16).toFloat, p(17).toFloat)).toDF

  //注册表
  seaWeed.registerTempTable("sea_weed")

  //使用sql语句操作表
  val t = sqlContext.sql("select season from sea_weed")
}