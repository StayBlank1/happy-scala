package test

/*����spark��Word2Vec*/
object scala1 extends App {
  import org.apache.spark.mllib.feature.Word2Vec
	val input = sc.textFile("F:/BaiduYunDownload/test.txt").map(line => line.split("\t").toSeq)
	val word2vec = new Word2Vec()
	val model = word2vec.fit(input) 
	val synonyms = model.findSynonyms("Emma", 40)
	synonyms.foreach{case (synonym, cosineSimilarity) => printf("%-20s %-20.16f\n",synonym, cosineSimilarity)}
}